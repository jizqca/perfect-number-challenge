# Perfect Number Challenge #

We would like you to write a function in PHP to determine whether a given number is [perfect](https://en.wikipedia.org/wiki/Perfect_number), [abundant](https://en.wikipedia.org/wiki/Perfect_number), or [deficient](https://en.wikipedia.org/wiki/Deficient_number). This is based on the classification scheme for natural numbers by [Nicomachus](https://en.wikipedia.org/wiki/Nicomachus).
This should then be exposed via an API using HTTP&JSON to the user.

### Perfect, Abundant, and Deficient ###

Whether a number is one of these three categories is based on that number's [aliquot sum](https://en.wikipedia.org/wiki/Aliquot_sum). The aliquot sum is calculated by the sum of the divisors or factors of a number, not including the number itself.

For example, the proper divisors of 15 (that is, the positive divisors of 15 that are not equal to 15) are 1, 3 and 5, so the aliquot sum of 15 is 9 (1 + 3 + 5).

* **A perfect number** is where the aliquot sum = number
  * 6 is a perfect number because (1 + 2 + 3) = 6
* **An abundant number** is where the aliquot sum > number
  * 12 is an abundant number because (1 + 2 + 3 + 4 + 6) = 16
* **A deficient number** is where the aliquot sum < number
  * 8 is a deficient number because (1 + 2 + 4) = 7

### Task ###

Write a function to determine whether a given number is perfect, abundant, or deficient:

```php
<?php
function getClassification($integer) {
    // returns 'perfect', 'abundant', or 'deficient'
}
```

Once you have created your function to check a perfect number, we would like you to create an API using HTTP and JSON to expose this functionality. You may structure the API in the way you see fit.

### Tips ###

When completing the task, please think about:

- object-oriented structure
- performance
- exception handling
- testing
- documentation

### Technical approach ###

To implement this task I've decided to use Symfony Microkernel as it offers the basic functionality needed for this task such as routing whilst still keeping it really lightweight.

### How to get the application running ###

You will need to:

- Checkout the code into your local machine. Bear in mind that it requires PHP 7
- Run `composer install` to install any needed dependencies
- `cd public`
- Run `php -S localhost:8000`
- You will be able to access the results by visiting this endpoint http://localhost:8000/operation/classify/{number} where {number} is the input that will be passed to getClassification. For example: http://localhost:8000/operation/classify/6

### How to run the tests ###

In order to run the unit tests, you just need to run `php vendor/bin/phpunit` from the root folder of the project.
