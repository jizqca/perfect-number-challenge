<?php

namespace App\Tests\Util;

use App\Util\Aliquot;
use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Exception;

class AliquotTest extends TestCase
{
	/**
	 * Test with valid parameters
	 */
    public function testGetClassification()
    {
        $aliquot = new Aliquot();

        // assert that:
	 	// - 6 is a perfect number
		// - 12 is an abundant number
	 	// - 8 is a deficient number
        $this->assertEquals('perfect', $aliquot->getClassification(6));
        $this->assertEquals('abundant', $aliquot->getClassification(12));
        $this->assertEquals('deficient', $aliquot->getClassification(8));
    }

    /**
     * Test with a string
     *
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage The argument needs to be an integer higher than 0
     */
    public function testGetClassificationWithString()
    {
        $aliquot = new Aliquot();
        $this->assertEquals('', $aliquot->getClassification('string'));
    }

    /**
     * Test with a decimal
     *
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage The argument needs to be an integer higher than 0
     */
    public function testGetClassificationWithDecimal()
    {
        $aliquot = new Aliquot();
        $this->assertEquals('', $aliquot->getClassification(10.54));
    }

    /**
     * Test with a negative number
     *
     * @expectedException InvalidArgumentException
     * @expectedExceptionMessage The argument needs to be an integer higher than 0
     */
    public function testGetClassificationWithNegativeNumber()
    {
        $aliquot = new Aliquot();
        $this->assertEquals('', $aliquot->getClassification(-110));
    }
}
