<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Util\Aliquot;

class OperationController extends Controller
{
    /**
     * @Route("/operation/classify/{number}", requirements={"number"="\d+"})
     */
    public function classify($number)
    {
        return new JsonResponse(array(
            'number' => $number,
            'classification' => Aliquot::getClassification($number)
        ));
    }
}