<?php

namespace App\Util;

use Symfony\Component\DependencyInjection\Exception\InvalidArgumentException;

class Aliquot
{
    /**
     * Returns the classification of a number based on their aliquot sum
	 *
     * A perfect number is where the aliquot sum = number
	 * - 6 is a perfect number because (1 + 2 + 3) = 6
	 * An abundant number is where the aliquot sum > number
	 * - 12 is an abundant number because (1 + 2 + 3 + 4 + 6) = 16
	 * A deficient number is where the aliquot sum < number
	 * - 8 is a deficient number because (1 + 2 + 4) = 7
     *
     * @param int $integer
     *
     * @return string
     */
    public function getClassification($integer): string
    {
        if (!is_numeric($integer) || $integer < 1 || is_double($integer)) {
            throw new InvalidArgumentException('The argument needs to be an integer higher than 0');
        }

        // 1 is a factor of all numbers, no need to check it
        $sum = 1;
        $classification = '';

        // we only need to check up to half the number
        for ($i = 2; $i <= floor($integer / 2); $i++) {
            if ($integer % $i == 0) {
                $sum += $i;
            }
        }
        
        if ($sum == $integer || $integer == 0) {
            $classification = 'perfect';
        } elseif ($sum > $integer) {
            $classification = 'abundant';
        } else {
            $classification = 'deficient';
        }

        return $classification;
    }
}
